# DevOps environment

This is a set of scripts to build a environment that will store, build, test and deploy a php application.
Gitlab and Openshift are used.


## Requirements

This solution was develeped and tested over:

- Ubuntu 18.04 LTS
- KVM support (https://help.ubuntu.com/community/KVM/Installation)


## Installation

Run the first script to install and configure the Gitlab:
```bash
sudo ./gitlab.sh
```

So, run the follow script to build the Openshift VM:
```bash
sudo ./openshift.sh
```

And the last you must run the script that will create and configure the application:
```bash
sudo ./application.sh
```

** IMPORTANT **
For the deploy works, you must change the yaml file **.gitlab-ci.yml in gitlab repository**, at:
```
staging:
  stage: deploy
  script:
    - curl -X POST -k [$URL]
```
The secret can be obtained from **Openshift -> Builds -> todo-backend-laravel -> Configuration -> Generic Webhook URL**.


In the end you should be able to access:

Gitlab
> http://localhost:7780
> User: root
> Password: gitpass123

Openshift
> https://localhost:8443/console
> User: admin
> Password: admin


## Testing

The CI/CD process can be seen in:
> http://localhost:7780/root/todo-backend-laravel/pipelines

And the deploy:
> https://localhost:8443/console/project/myproject/overview

