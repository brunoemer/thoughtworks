#!/bin/bash

# dependecies
echo "Installing python gitlab tool"
apt update
apt install -y python3-pip
pip3 install python-gitlab

echo "[global]
default = main
ssl_verify = false
timeout = 5

[main]
url = http://localhost
api_version = 4
private_token = siZaM7d2fQkjKFLzy5f6
" > /etc/python-gitlab.cfg

# docker
echo "Installing gitlab runner"
apt install -y apt-transport-https ca-certificates curl software-properties-common gnupg2
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian stretch stable"
apt update
apt install -y docker-ce

# runner
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh >/tmp/gitlab-runner.sh && 
	chmod +x /tmp/gitlab-runner.sh && 
	/tmp/gitlab-runner.sh
	
apt update
apt-get install -y gitlab-runner

gitlab-runner register -u http://gitlab.local/ -r y2tag8RHJH2ZPs6JgKe5 -n --executor docker --docker-image lorisleiva/laravel-docker --docker-volumes '/var/run/docker.sock:/var/run/docker.sock' --docker-volumes '/etc/hosts:/etc/hosts'


