#!/bin/bash

# application example
echo "Creating gitlab project"
/opt/gitlab/embedded/bin/gitlab project create --name todo-backend-laravel --visibility public

echo "Cloning test application: todo-backend-laravel"
git clone https://github.com/janRedmann/todo-backend-laravel

cd todo-backend-laravel

git remote set-url origin http://root:gitpass123@gitlab.local/root/todo-backend-laravel.git

echo "Creating pipeline and env variables"
cp /tmp/gitlab/.gitlab-ci.yml .

cp /tmp/gitlab/.env.example .

git add .
git commit -m "Add .gitlab-ci.yml and .env"
git push

