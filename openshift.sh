#!/bin/bash

# https://docs.okd.io/latest/minishift/getting-started/installing.html

echo "Installing openhisft dependencies"
apt update
apt install -y libvirt-bin qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils

if ! type "minishift" > /dev/null; then

	echo "Installing minishift"
	curl -L https://github.com/dhiltgen/docker-machine-kvm/releases/download/v0.10.0/docker-machine-driver-kvm-ubuntu16.04 -o /usr/local/bin/docker-machine-driver-kvm
	chmod +x /usr/local/bin/docker-machine-driver-kvm

	wget -O /tmp/minishift.tgz https://github.com/minishift/minishift/releases/download/v1.34.2/minishift-1.34.2-linux-amd64.tgz

	tar -xzf /tmp/minishift.tgz -C /tmp/

	mv /tmp/minishift-1.34.2-linux-amd64/minishift /usr/local/bin/

	usermod -a -G libvirt $(whoami)

fi

minishift start

# workaround login bug
echo "Fixing permissions"
minishift ssh 'export KUBECONFIG=/var/lib/minishift/base/openshift-apiserver/admin.kubeconfig PATH="$PATH:/var/lib/minishift/bin"; oc whoami; oc adm policy add-cluster-role-to-user cluster-admin admin; oc adm policy add-cluster-role-to-user cluster-admin system:serviceaccount:default:deployer'

# production config
ip=$(minishift ip)
grep "openshift.local" /etc/hosts >/dev/null || echo "$ip      openshift.local" >> /etc/hosts

# proxy
echo "Redirect to openshift vm, enter the password of $USER:"
ssh -fN -L 0.0.0.0:8443:$ip:8443 -L 0.0.0.0:80:$ip:80 $USER@localhost

echo "Logging openshift"
minishift oc-env

eval $(minishift oc-env)

oc login https://$ip:8443 -u admin -p admin --insecure-skip-tls-verify=true

oc rollout latest dc/docker-registry -n default
oc rollout latest dc/router -n default


