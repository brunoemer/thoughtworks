#!/bin/bash

# gitlab
docker cp gitlab/ thoughtworks_web_1:/tmp/
docker exec -w /tmp/gitlab/ thoughtworks_web_1 /bin/sh -c './gitlab_app.sh'


# openshift
ip=$(minishift ip)

minishift oc-env

eval $(minishift oc-env)

echo "Logging in openshift"
oc login https://$ip:8443 -u admin -p admin --insecure-skip-tls-verify=true

echo "Creating the application resources"
oc new-app http://root:gitpass123@192.168.42.1:7780/root/todo-backend-laravel.git \
        --name todo-backend-laravel --image-stream=php --env-file=gitlab/.env.example

oc set deployment-hook dc/todo-backend-laravel --pre -c todo-backend-laravel --failure-policy=abort -- /bin/sh -c 'touch database/database.sqlite; php artisan migrate'

oc expose svc/todo-backend-laravel

echo "Opening in browser"
minishift openshift service todo-backend-laravel --in-browser

