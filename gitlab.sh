#!/bin/bash

# https://docs.gitlab.com/omnibus/docker/

echo "Installing gitlab dependencies"
apt update
apt install -y docker docker-compose

rm -rf /srv/gitlab/
grep "gitlab.local" /etc/hosts >/dev/null || echo "172.17.0.2      gitlab.local" >> /etc/hosts

echo "Starting the docker"
docker-compose up -d

ready=0
while [ $ready -eq 0 ]; do
	echo "Waiting for docker get ready"
	ready=$(docker ps -q -f name=thoughtworks_web_1 -f status=running -f health=healthy |wc -l)
	sleep 5
done

if [ $ready -gt 0 ]; then

	docker cp gitlab/ thoughtworks_web_1:/tmp/
	docker exec -w /tmp/gitlab/ thoughtworks_web_1 /bin/sh -c './project.sh'

fi

